import React from "react";
import { Theme, ThemeContext } from "./theme";

type Interaction = {
  hover: boolean;
  focus: boolean;
  active: boolean;
  key: string | null;
};

type Interactise<T> = T | ((i: Interaction, f: Interaction, t: Theme) => T);

const InteractionContext = React.createContext<Interaction>({
  hover: false,
  focus: false,
  active: false,
  key: null
});

export const WithAppearance: React.FunctionComponent<{
  className?: Interactise<string>;
  style?: Interactise<React.CSSProperties>;
}> = p => {
  const ctx = React.useContext(InteractionContext);
  const theme = React.useContext(ThemeContext);
  const [hover, set_hover] = React.useState(false);
  const [focus, set_focus] = React.useState(false);
  const [active, set_active] = React.useState(false);
  const [key, set_key] = React.useState<string | null>(null);
  const children = React.Children.map(p.children, c => {
    if (!React.isValidElement(c)) {
      return c;
    }

    if (typeof c.type !== "string") {
      return c;
    }

    return React.cloneElement(c, {
      ...c.props,
      className:
        typeof p.className === "function"
          ? p.className({ hover, focus, active, key }, ctx, theme)
          : p.className,
      style:
        typeof p.style === "function"
          ? p.style({ hover, focus, active, key }, ctx, theme)
          : p.style,
      onFocus: (e: any) => {
        set_focus(true);
        c.props.onFocus && c.props.onFocus(e);
      },
      onBlur: (e: any) => {
        set_focus(true);
        c.props.onBlur && c.props.onBlur(e);
      },
      onMouseEnter: (e: any) => {
        set_hover(true);
        c.props.onMouseEnter && c.props.onMouseEnter(e);
      },
      onMouseLeave: (e: any) => {
        set_hover(false);
        c.props.onMouseLeave && c.props.onMouseLeave(e);
      },
      onMouseDown: (e: any) => {
        set_active(true);
        c.props.onMouseDown && c.props.onMouseDown(e);
      },
      onMouseUp: (e: any) => {
        set_active(false);
        c.props.onMouseUp && c.props.onMouseUp(e);
      },
      onKeyDown: (e: React.KeyboardEvent) => {
        set_key(e.key);
        c.props.onKeyDown && c.props.onKeyDown(e);
      },
      onKeyUp: (e: React.KeyboardEvent) => {
        set_key(null);
        c.props.onKeyUp && c.props.onKeyUp(e);
      }
    });
  });

  return <>{children}</>;
};

export const ProvideInteractions: React.FunctionComponent = p => {
  const [hover, set_hover] = React.useState(false);
  const [focus, set_focus] = React.useState(false);
  const [active, set_active] = React.useState(false);
  const [key, set_key] = React.useState<string | null>(null);
  const children = React.Children.map(p.children, c => {
    if (!React.isValidElement(c)) {
      return c;
    }

    if (typeof c.type !== "string") {
      return c;
    }

    return React.cloneElement(c, {
      ...c.props,
      onFocus: (e: any) => {
        set_focus(true);
        c.props.onFocus && c.props.onFocus(e);
      },
      onBlur: (e: any) => {
        set_focus(true);
        c.props.onBlur && c.props.onBlur(e);
      },
      onMouseEnter: (e: any) => {
        set_hover(true);
        c.props.onMouseEnter && c.props.onMouseEnter(e);
      },
      onMouseLeave: (e: any) => {
        set_hover(false);
        c.props.onMouseLeave && c.props.onMouseLeave(e);
      },
      onMouseDown: (e: any) => {
        set_active(true);
        c.props.onMouseDown && c.props.onMouseDown(e);
      },
      onMouseUp: (e: any) => {
        set_active(false);
        c.props.onMouseUp && c.props.onMouseUp(e);
      },
      onKeyDown: (e: React.KeyboardEvent) => {
        set_key(e.key);
        c.props.onKeyDown && c.props.onKeyDown(e);
      },
      onKeyUp: (e: React.KeyboardEvent) => {
        set_key(null);
        c.props.onKeyUp && c.props.onKeyUp(e);
      }
    });
  });

  return (
    <InteractionContext.Provider value={{ hover, focus, active, key }}>
      {children}
    </InteractionContext.Provider>
  );
};

export function WithCustomisation<TProps, TStyles>(
  component: React.FunctionComponent<
    TProps & { customisations: Partial<TStyles> }
  >
): React.FunctionComponent<TProps> & {
  Customise: (
    customiser: (theme: Theme, props: TProps) => Partial<TStyles>
  ) => void;
} {
  let customiser = (theme: Theme, props: TProps) => ({});
  function Customise(
    customiser: (theme: Theme, props: TProps) => Partial<TStyles>
  ) {
    customiser = customiser;
  }

  const result: React.FunctionComponent<TProps> = p => {
    const theme = React.useContext(ThemeContext);
    const c = customiser(theme, p);
    return React.createElement(
      component,
      { ...p, customisations: c },
      p.children
    );
  };

  return Object.assign(result, { Customise });
}
