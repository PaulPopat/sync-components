import React from "react";
import { Theme, Colour } from "./theme";
import { WithAppearance, WithCustomisation } from "./html";

export const Button = WithCustomisation<
  {
    colour: keyof Theme["colour"]["theme"];
    disabled?: boolean;
  } & (
    | {
        type: "button" | "reset";
        onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
      }
    | {
        type: "submit";
      }
  ),
  {}
>(p => (
  <WithAppearance
    style={(i, f, t) => {
      const background = Colour(t.colour.theme[p.colour]);
      return {
        background: background.render(),
        color:
          background.brightness() > 2.5
            ? t.colour.grey.black
            : t.colour.grey.white,
        border: t.border.style,
        borderRadius: t.border.radius.large,
        outline: "none",
        fontFamily: t.font.family,
        fontSize: t.font.size.medium,
        fontWeight: t.font.weight.normal,
        padding: "0.875rem 1rem",
        boxShadow: i.active
          ? `0 0 1px 2px ${background.withAlpha(0.8).render()}`
          : i.hover || i.focus
          ? `0 0 1px 3px ${background.withAlpha(0.5).render()}`
          : undefined,
        cursor: "pointer",
        transition: "box-shadow 150ms"
      };
    }}
  >
    <button type={p.type} disabled={p.disabled}>
      {p.children}
    </button>
  </WithAppearance>
));
