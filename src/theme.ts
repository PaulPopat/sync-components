import React from "react";

export const DefaultTheme = {
  colour: {
    theme: {
      action: "#cd3a72",
      info: "#3c151d",
      success: "#738c5a",
      warning: "#f07c22",
      danger: "#f44336"
    },
    grey: {
      black: "#111211",
      dark: "#333",
      light: "#999",
      white: "#fffff6"
    }
  },
  font: {
    family: "'Muli', sans-serif",
    weight: {
      thin: 300,
      normal: 500,
      bold: 600
    },
    size: {
      small: "0.6875rem",
      medium: "0.875rem",
      large: "1.125rem"
    }
  },
  border: {
    style: "none",
    radius: {
      small: "0.5rem",
      medium: "1rem",
      large: "2rem"
    }
  }
};

export type Theme = typeof DefaultTheme;

export const ThemeContext = React.createContext(DefaultTheme);

export function Colour(
  colour: string | { r: number; g: number; b: number; a: number }
) {
  const parse = (c: string) => {
    if (c.startsWith("#")) {
      if (c.length === 4) {
        return {
          r: parseInt(c[1], 16),
          g: parseInt(c[2], 16),
          b: parseInt(c[3], 16),
          a: 1
        };
      } else if (c.length === 7) {
        return {
          r: parseInt(c[1] + c[2], 16),
          g: parseInt(c[3] + c[4], 16),
          b: parseInt(c[5] + c[6], 16),
          a: 1
        };
      }
    } else if (c.startsWith("rgb(")) {
      const parts = c
        .replace("rgb(", "")
        .replace(")", "")
        .split(",")
        .map(s => s.trim());
      return {
        r: parseInt(parts[0]),
        g: parseInt(parts[1]),
        b: parseInt(parts[2]),
        a: 1
      };
    } else if (c.startsWith("rgba(")) {
      const parts = c
        .replace("rgba(", "")
        .replace(")", "")
        .split(",")
        .map(s => s.trim());
      return {
        r: parseInt(parts[0]),
        g: parseInt(parts[1]),
        b: parseInt(parts[2]),
        a: parseFloat(parts[3])
      };
    }

    throw new Error("Invalid colour");
  };

  const data = typeof colour === "string" ? parse(colour) : colour;
  return {
    render() {
      return `rgba(${data.r}, ${data.g}, ${data.b}, ${data.a})`;
    },
    brightness() {
      return Math.sqrt(
        ((0.299 * (data.r / 255)) ^ 2) +
          ((0.587 * (data.g / 255)) ^ 2) +
          ((0.114 * (data.b / 255)) ^ 2)
      );
    },
    withAlpha(a: number) {
      return Colour({ ...data, a });
    }
  };
}
