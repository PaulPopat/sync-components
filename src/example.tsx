import React from "react";
import ReactDOM from "react-dom";
import { Button } from "./buttons";

ReactDOM.render(
  <Button colour="action" type="submit">
    Hello world
  </Button>,
  document.getElementById("target")
);
